package com.stgconsulting.bab.buns.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Bun model.
 * Reference Images site: https://www.saveur.com/gallery/A-Guide-to-Hamburger-Meat/
 */
@Data
@NoArgsConstructor
@Entity
public class Bun {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", required = true, dataType = "Long", example = "123")
    private Long id;

    @ApiModelProperty(value = "name", required = true, dataType = "String", example = "Sliced Bread")
    private String name;

    @ApiModelProperty(value = "description", required = true, dataType = "String", example = "This is one of the most flavorful cuts for burgers. Look for brisket labeled \"flat cut\" if you like a leaner grind; ask for the fattier \"second cut\" or \"nose cut\" for all-out richness.")
    private String description;

    @ApiModelProperty(value = "image_path", required = true, dataType = "String", example = "https://www.saveur.com/resizer/9plJj8-iMgdCN6go1NKN12xy9SE=/400x287/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/5E2BJVQGVGOOLUOPBHC3H3RJX4.jpg")
    @JsonProperty(value = "image_path")
    private String imagePath;
}
