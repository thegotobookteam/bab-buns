package com.stgconsulting.bab.buns.model.constants;

import static com.stgconsulting.bab.buns.model.constants.Headers.BUN_ID;

/**
 * Resource constants.
 *
 * Note:
 * By convention, such fields have names consisting of capital letters, with words separated by underscores.
 * It is critical that these fields contain either primitive values or references to immutable objects.
 */
public class Resources {

    public static final String REST_PREFIX = "/rest";
    public static final String BASE_RESOURCE = "/buns";

    public static final String API_BUNS_ALL_URI = BASE_RESOURCE;
    public static final String API_BUNS_ALL_DESC = "Retrieve list of all hamburger buns.";

    public static final String API_BUN_BY_ID_URI = BASE_RESOURCE + "/{" + BUN_ID + "}";
    public static final String API_BUN_BY_ID_DESC = "Retrieve details of a hamburger bun.";

    public static final String API_BUN_CREATE_URI = BASE_RESOURCE;
    public static final String API_BUN_CREATE_DESC = "Create a hamburger bun.";

    public static final String API_BUN_UPDATE_URI = BASE_RESOURCE;
    public static final String API_BUN_UPDATE_DESC = "Update a hamburger bun information.";

    public static final String API_BUN_DELETE_BY_ID_URI = BASE_RESOURCE + "/{" + BUN_ID + "}";
    public static final String API_BUN_DELETE_BY_ID_DESC = "Delete a hamburger bun.";

    public static final String API_BUN_IMAGE_BY_ID_URI = BASE_RESOURCE + "/{" + BUN_ID + "}/image";
    public static final String API_BUN_IMAGE_BY_ID_DESC = "Retrieve bun image.";

    public static final String API_BUN_IMAGE_UPLOAD_BY_ID_URI = BASE_RESOURCE + "/{" + BUN_ID + "}/image";
    public static final String API_BUN_IMAGE_UPLOAD_BY_ID_DESC = "Upload a bun image by bun identifier.";

}
