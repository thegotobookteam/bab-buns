@echo off
rem -------------------------------------------------------------------
rem Execute Build-A-Burger micro-service
rem -------------------------------------------------------------------
echo Executing Build-A-Burger micro-service...
docker run --rm -p 8001 --network host --name bab-buns bab-buns-web:1.LOCAL
