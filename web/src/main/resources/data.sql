-- https://www.saveur.com/article/Techniques/Building-the-Perfect-Burger/
-- Resource site: https://www.saveur.com/gallery/Classic-Buns-and-Breads/
DROP TABLE IF EXISTS buns;

CREATE TABLE buns
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(250)  NOT NULL,
    description VARCHAR(4096) NOT NULL,
    image_path  VARCHAR(250) DEFAULT NULL
);

INSERT INTO buns (name, description, image_path)
VALUES ('Ciabatta Roll',
        'The thick crust of Italian-style ciabatta provides structure for even the juiciest of burgers. The ciabatta rolls made by Amy''s Bread in New York City are the gold standard.',
        'https://www.saveur.com/resizer/ksxhCYmsDmR51JWX7ADuuNbBtFc=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/WRWWZLDYKD6ZJ7Y7KTI4DWWIYU.jpg'),
       ('English Muffin',
        'Round and sturdy, the English muffin seems designed for burgers. Wolferman''s, a bakery in Hope, Arkansas, makes a miniature muffin that''s ideal for sliders. Toast it first.',
        'https://www.saveur.com/resizer/_c5fXI6nJ0GSy84kfrtz7dlmfOA=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/C6DO5Z6LCGMKPA4SZRT5A6LBEI.jpg'),
       ('Kaiser Roll',
        'The bulk and fluffy interior of this classic bun, good versions of which can be found in most supermarkets, make it an excellent vehicle for big, juicy backyard burgers.',
        'https://www.saveur.com/resizer/QeY6tgJHsZ3y3oyHUge6qlwTBT8=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/HBKNRDEDGTFA2CTISPTB4FBWYA.jpg'),
       ('Onion Roll',
        'A bun studded with onion bits gives character to simple burgers. Farm to Market Bread Co. of Kansas City, Missourri, makes a terrific onion brioche.',
        'https://www.saveur.com/resizer/rC1CYB2RRvLPPZcT6WF4a7tuz58=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/62A6LJDPOQ2Y3VIJMFVRAJDPU4.jpg'),
       ('Potato Roll',
        'This sweet, pillowy roll is the softest of burger buns. Martin''s in Chambersburg, Pennsylvania, makes the definitive version, sold in supermarkets along the East Coast.',
        'https://www.saveur.com/resizer/MXtUBHpSKAgZsO36xyDKt0aTFRo=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/JQUG76OS4FRONMN4ZROTPWAWMI.jpg'),
       ('Pretzel Roll',
        'Salty and chewy, these rolls, made by J & J Snack Foods in Pennsauken, New Jersey, are sold nationwide.',
        'https://www.saveur.com/resizer/n7aBYhDZWxGjcw6b35rlyc-sNHg=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/QF3LF2O35VTWWMZH5PZ66OHH2A.jpg'),
       ('Sesame Seed Bun',
        'This soft and fragrant bun has become the iconic hamburger platform. Arnold Bakery of Horsham, Pennsylvania, makes one of our favorite models.',
        'https://www.saveur.com/resizer/AxxhQTFQrcTvCd4ZyX7yfqMzbJc=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/GPYSWNNCZXH5SZT6YKCQXU4YY4.jpg'),
       ('Sliced Bread',
        'Toasted sandwich bread was the original burger bun. Denser-textured breads, like the ones from Pepperidge Farm, hold up well to patties thick and thin.',
        'https://www.saveur.com/resizer/5hpXqro-RhGK4TrL_uJC2qMVO7s=/400x400/cloudfront-us-east-1.images.arcpublishing.com/bonnier/PEWT3B5P4GLSFO6NRZDP47PHEM.jpg');

