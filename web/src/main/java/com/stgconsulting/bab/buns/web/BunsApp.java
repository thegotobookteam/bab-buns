package com.stgconsulting.bab.buns.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

/**
 * Application driver.
 * <p>
 * o Docker run:  docker run --rm -p 8001 --name bab-buns bab-buns-web:1.LOCAL
 * o Docker run:  docker run --rm -p 8001 --network host --name bab-buns bab-buns-web:1.LOCAL
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@PropertySource("classpath:build.properties")
public class BunsApp {

    public static void main(String[] args) {
        log.info("main() :: Enter");

        SpringApplication.run(BunsApp.class, args);

        log.info("main() :: Exit");
    }
}
