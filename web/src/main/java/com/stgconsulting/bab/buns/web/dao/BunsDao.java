package com.stgconsulting.bab.buns.web.dao;

import com.stgconsulting.bab.buns.model.Bun;
import com.stgconsulting.bab.buns.web.util.FileUtils;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

/**
 * Buns DAO or Data Access Object.
 */
@Slf4j
@Repository
@NoArgsConstructor
public class BunsDao {

    private JdbcTemplate jdbcTemplate;
    private String imagesPath;

    @Autowired
    public void setDatasource(final DataSource pDataSource) {
        this.jdbcTemplate = new JdbcTemplate(pDataSource);
        final CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
        jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);
        imagesPath = getClass().getClassLoader().getResource("images").getPath().concat("/");
    }

    public int getCount() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM BUNS", Integer.class);
    }

    public List<Bun> findAll() {
        return jdbcTemplate.query("SELECT * FROM BUNS", new BunsRowMapper());
    }

    public Bun findOne(final long pBunId) {
        final String query = "SELECT * FROM BUNS WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{pBunId}, new BunsRowMapper());
    }

    public Integer create(final Bun pBun, MultipartFile pMultipartFile) {
        Integer result;
        try {
            result = uploadImage(pBun, pMultipartFile);
        } catch (IOException e) {
            log.error("Error saving image: " + e.getLocalizedMessage());
            result = 0;
        }
        // If image upload successful, create record
        if (result > 0) {
            boolean hasDefinedImage = StringUtils.isNotBlank(pBun.getImagePath());
            String fileName = hasDefinedImage ? pBun.getImagePath() : pMultipartFile.getOriginalFilename();
            result = jdbcTemplate.update("INSERT INTO BUNS VALUES (?, ?, ?, ?)",
                    pBun.getId(), pBun.getName(), pBun.getDescription(), fileName);
        }
        return result;
    }

    public Integer update(final Bun pBun) {
        return jdbcTemplate.update("UPDATE BUNS SET name = ?, description = ?, file_image = ? WHERE ID = ?",
                pBun.getName(), pBun.getDescription(), pBun.getImagePath(), pBun.getId());
    }

    public Integer delete(final Long pId) {
        return jdbcTemplate.update("DELETE FROM BUNS WHERE ID = ?", pId);
    }

    private Integer uploadImage(final Bun pBun, MultipartFile pFile) throws IOException {
        Integer result = 0;
        boolean hasDefinedImage = StringUtils.isNotBlank(pBun.getImagePath());
        String fileName = hasDefinedImage ? pBun.getImagePath() : pFile.getOriginalFilename();
        if (pFile.getContentType().contains("image/")) {
            FileUtils.createFolderIfNotExists(imagesPath);
            FileUtils.saveToFile(pFile.getInputStream(), imagesPath + fileName);
            result = 1;
        }
        return result;
    }
}
