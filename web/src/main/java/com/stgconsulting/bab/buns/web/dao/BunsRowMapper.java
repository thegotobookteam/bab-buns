package com.stgconsulting.bab.buns.web.dao;

import com.stgconsulting.bab.buns.model.Bun;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bun Row Mapper class.
 */
public class BunsRowMapper implements RowMapper<Bun> {

    @Override
    public Bun mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Bun bun = new Bun();

        bun.setId(resultSet.getLong("ID"));
        bun.setName(resultSet.getString("NAME"));
        bun.setDescription(resultSet.getString("DESCRIPTION"));
        bun.setImagePath(resultSet.getString("IMAGE_PATH"));

        return bun;
    }
}
