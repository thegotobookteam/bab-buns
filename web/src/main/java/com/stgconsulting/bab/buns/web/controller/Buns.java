package com.stgconsulting.bab.buns.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.stgconsulting.bab.buns.model.Bun;
import com.stgconsulting.bab.buns.web.service.BunService;
import com.stgconsulting.bab.buns.web.util.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.stgconsulting.bab.buns.model.constants.Headers.BUN_ID;
import static com.stgconsulting.bab.buns.model.constants.Resources.*;

@Slf4j
@RestController
@RequestMapping(REST_PREFIX)
class Buns {

    private BunService bunService;
    private ObjectMapper objectMapper;

    /**
     * DI Constructor
     */
    public Buns(BunService pBunService) {
        bunService = pBunService;
        objectMapper = new ObjectMapper();
    }

    @GetMapping(value = API_BUNS_ALL_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bun> findAll() {
        return bunService.findAll();
    }

    @GetMapping(value = API_BUN_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public Bun findById(@Valid @PathVariable(BUN_ID) Long pBunId) {
        return RestPreconditions.checkFound(bunService.findOne(pBunId));
    }

    @PostMapping(value = API_BUN_CREATE_URI, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Integer create(@RequestParam(required = true, value = "bunJson") String pBunJson,
                          @RequestParam(required = true, value = "image") MultipartFile pMultipartFile) {
        Bun bun = null;
        try {
            bun = objectMapper.readValue(pBunJson, Bun.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(bun);
        return bunService.create(bun, pMultipartFile);
    }

    @PutMapping(value = API_BUN_UPDATE_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Bun pBun) {
        Preconditions.checkNotNull(pBun);
        RestPreconditions.checkFound(bunService.findOne(pBun.getId()));
        bunService.update(pBun);
    }

    @DeleteMapping(value = API_BUN_DELETE_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable(BUN_ID) Long pId) {
        bunService.deleteById(pId);
    }

    @GetMapping(value = API_BUN_IMAGE_BY_ID_URI, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    byte[] getImage(@Valid @PathVariable(BUN_ID) Long pBunId) throws IOException {
        Bun bun = bunService.findOne(pBunId);
        InputStream in = getClass().getResourceAsStream("/images/" + bun.getImagePath());
        return IOUtils.toByteArray(in);
    }

//    @PostMapping(value = API_BUN_IMAGE_UPLOAD_BY_ID_URI, produces = MediaType.IMAGE_JPEG_VALUE)
//    @ResponseStatus(HttpStatus.OK)
//    public Integer uploadImage(@Valid @PathVariable(BUN_ID) Long pBunId,
//                                              @RequestParam("image") MultipartFile file) {
//        log.info("uploadImage() :: Enter/Exit");
//        return bunService.uploadImage(pBunId, file);
//    }
}
