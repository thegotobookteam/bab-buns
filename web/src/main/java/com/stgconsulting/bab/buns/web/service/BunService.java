package com.stgconsulting.bab.buns.web.service;

import com.stgconsulting.bab.buns.model.Bun;
import com.stgconsulting.bab.buns.web.dao.BunsDao;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Bun service.
 */
@Service
public class BunService {

    private BunsDao bunsDao;

    public BunService(BunsDao bunsDao) {
        this.bunsDao = bunsDao;
    }

    public Bun findOne(Long pId) {
        return bunsDao.findOne(pId);
    }

    public List<Bun> findAll() {
        return bunsDao.findAll();
    }

    public Integer create(Bun pBun, MultipartFile pMultipartFile) {
        return bunsDao.create(pBun, pMultipartFile);
    }

    public Integer update(Bun pBun) {
        return bunsDao.update(pBun);
    }

    public Integer deleteById(Long pId) {
        return bunsDao.delete(pId);
    }

}
